@component($typeForm, get_defined_vars())
    <div class="fmf"  data-controller="fmf" data-field-name="{{ $name }}">
        <style>
            .btn.add {
                margin: 10px 0;
            }
            .file_row {
                margin: 10px 5px;
                display: inline-block;
                border: 1px solid var(--bs-border-color);
                padding: 10px;
            }
            .file_row div.img_box {
                display: inline-block;
                position: relative;
                width: 100px;
                height: 100px;
                overflow: hidden;
                background-repeat:no-repeat;
                background-position: center center;
                background-size: cover;
            }
            .file_row .btn.remove {
                width: 84px;
                margin: auto;
            }

        </style>

        <button data-action="click->fmf#showFM" type="button" class="btn btn-dark add">Добавить фото</button>
        <div class="border p-3 ds-{{$id}}" id="fmf-wrapper-{{$id}}" style="min-height: {{ $attributes['height'] }}">
            <span data-fmf-target="output" class="file_list"  name="{{ $name }}" data-field-name="{{ $name }}" data-multiple="{{isset($multiple) ? 1 : 0}}">
            @if($value !== '' && !is_null($value) )
                    @foreach (explode(",", $value) as $image_key => $image)

                        <div class="file_row" id="file_row_-{{$image_key }}" data-index="-{{$image_key }}">
                        <div class="img_box" style="background-image: url(&quot;{{$image}}&quot;);"></div>
                        <input name="{{ $name }}" hidden="hidden" value="{{$image}}" data-is-new="0">
                        <button class="remove btn btn-danger" data-index="-{{$image_key }}">Удалить</button>
                    </div>
                    @endforeach
                @endif
            </span>
        </div>

        <!--<input data-fmf-target="name" type="text">
        <span data-action="click->fmf#al">test!</span>

        <button data-action="click->fmf#al">
            Greet
        </button>
        <div class="border p-3" id="fmf-wrapper-{{$id}}" style="min-height: {{ $attributes['height'] }}">
            <span data-fmf-target="output"></span>

            {!! $value !!}
        </div>
        <input type="hidden" > -->

    </div>
@endcomponent
