application.register("fmf", class extends window.Controller {
    fieldName = null;
    file_index = 0
    multiple = false

    connect() {
        this.fieldName = this.element.dataset.fieldName;
        const input = this.element.querySelector('input');

        let $this = this;
        window.fmSetLink = function ($url){
            $this.addFile($url)
        }

        this.multiple = this.outputTarget.dataset['multiple']

        window.fmRemoveHandler = function(e){
            e.preventDefault()
            const index = this.dataset.index;
            const elem = document.getElementById('file_row_' + index);
            elem.parentNode.removeChild(elem);
        }


        document.querySelectorAll('.fmf .file_row .remove').forEach(rmb => {
            rmb.addEventListener('click', window.fmRemoveHandler)
        });


    }

    static get targets() {
        return [ "name", "output" ]
    }
    showFM() {
        window.fmf_target_current = this.outputTarget;
        window.open('/file-manager/fm-button', 'fm', 'width=1400,height=800');
    }

    addFile($url) {
        $url = $url.replace(window.location.origin, '');

        this.file_index++;
        const row_file_index = this.file_index.toString()

        let row = document.createElement("div")
        row.classList.add("file_row")
        row.setAttribute('id', 'file_row_' + row_file_index)
        row.dataset.index = row_file_index


        let input = document.createElement("input")
        input.setAttribute('name', window.fmf_target_current.dataset['fieldName'])
        input.setAttribute('hidden', 'hidden')
        input.setAttribute('value', $url)
        input.dataset.isNew =  "1"

        let imgBox = document.createElement('div')
        imgBox.classList.add('img_box')
        imgBox.style.backgroundImage="url('" + $url + "')";

        let removeButton = document.createElement('button')
        removeButton.classList.add('remove')
        removeButton.classList.add('btn')
        removeButton.classList.add('btn-danger')
        removeButton.textContent = 'Удалить'
        removeButton.dataset.index = row_file_index
        removeButton.addEventListener('click', window.fmRemoveHandler)

        row.append(imgBox)
        row.append(input)
        row.append(removeButton)

        if(window.fmf_target_current.dataset['multiple'] === '0'){
            window.fmf_target_current.innerHTML = ''
        }
        window.fmf_target_current.append(row)

    }

    disconnect() {

    }
});
