<?php

namespace Dispace\OrchidFilemanagerField;

use App\Providers\RouteServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Orchid\Support\Facades\Dashboard;

class FMFServiceProvider extends ServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'orchid-filemanager-field');

        $this->callAfterResolving('view', static function (ViewFactory  $factory) {
            $factory->composer('platform::app', static function () {
                Dashboard::registerResource('scripts', vite('resources/vendor/dispace/fmf/fmf_controller.js'));
            });
        });

        $this->publishes([
            __DIR__.'/../resources/js' => resource_path('vendor/dispace/fmf'),
        ], ['fmf-assets', 'laravel-assets', 'orchid-assets']);
    }
}
