<?php

namespace Dispace\OrchidFilemanagerField;

use Orchid\Screen\Concerns\Multipliable;
use Orchid\Screen\Field;

class FileManagerField extends Field
{
    use Multipliable;

    /**
     * @var string
     */
    protected $view = 'orchid-filemanager-field::filemanager-field';

    /**
     * All attributes that are available to the field.
     *
     * @var array
     */
    protected $attributes = [
        'value'                     => null,
        'height'                    => '100px',
    ];

    /**
     * Attributes available for a particular tag.
     *
     * @var array
     */
    protected $inlineAttributes = [
        'value',
        'height',
    ];

    /**
     * @param array $config
     *
     * @return $this
     */
    public function config(array $config): self
    {
       // $this->set('config', json_encode($config));

        return $this;
    }

    /**
     * @param string|null $name
     *
     * @return Field
     */
    public static function make(string $name = null): Field
    {
        return (new static())->name($name);
    }
}
